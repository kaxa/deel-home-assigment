const {getProfile} = require('../middleware/getProfile')
const {Router} = require('express');
const app = Router();

/**
 * deposit money to user balance
 * @returns
 */
app.post('/balances/deposit/:userId', getProfile, async (req, res) => {

    const balanceDbRepository = req.app.get('balanceDbRepository')

    const {userId} = req.params
    const depositAmount = +req.body.amount;

    if (!depositAmount) res.status(400).json({message: "invalid amount"})

    const userProfile = req.profile.dataValues

    if (userProfile.type !== 'client') return res.status(403).json({message: 'invalid user'})

    if (+userProfile.id === +userId) return res.status(422).json({message: 'invalid user'})

    const result = await balanceDbRepository.depositToUser(userProfile.id, userId, depositAmount)

    if (result.completed) res.json()

    if (!result.completed) res.status(422).json({message: result.message})

})

module.exports = app;
