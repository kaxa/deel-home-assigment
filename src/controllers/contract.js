const {getProfile} = require('../middleware/getProfile')
const {Router} = require('express');
const app = Router();

/**
 * gets contract from database by id
 * checks contract owner
 * @returns contract by id
 */
app.get('/contracts/:id', getProfile, async (req, res) => {

    const contractDbRepository = req.app.get('contractDbRepository')
    const {id} = req.params
    const userProfile = req.profile.dataValues
    const contract = await contractDbRepository.getContract(id)
    if (!contract) return res.status(404).json({
        message: 'not found'
    })
    if (contract.dataValues.ClientId !== userProfile.id
        && contract.dataValues.ContractorId !== userProfile.id) {
        return res.status(403).json({
            message: 'no permission'
        })
    }
    res.json(contract)

})

/**
 * @returns user all unterminated contracts
 */
app.get('/contracts', getProfile, async (req, res) => {

    const userProfile = req.profile.dataValues
    const contractDbRepository = req.app.get('contractDbRepository')
    let result;

    if (userProfile.type === 'client') {
        result = await contractDbRepository.listClientUnTerminatedContracts(userProfile.id)
    }

    if (userProfile.type === 'contractor') {
        result = await contractDbRepository.listContractorUnTerminatedContracts(userProfile.id)
    }

    res.json(result)

})

module.exports = app;
