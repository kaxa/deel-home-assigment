const {getProfile} = require('../middleware/getProfile')
const {Router} = require('express');
const app = Router();

/**
 * @returns user unpaid jobs
 */
app.get('/jobs/unpaid', getProfile, async (req, res) => {

    const jobDbRepository = req.app.get('jobDbRepository')

    const unpaidJobsByUserType = {
        'client': jobDbRepository.getClientUnpaidJobs,
        'contractor': jobDbRepository.getContractorUnpaidJobs
    }

    const userProfile = req.profile.dataValues

    const result = await unpaidJobsByUserType[userProfile.type](userProfile.id)

    res.json(result)

})

/**
 * pays job for user
 * @returns
 */
app.post('/jobs/:job_id/pay', getProfile, async (req, res) => {

    const {job_id} = req.params

    const jobDbRepository = req.app.get('jobDbRepository')

    const userProfile = req.profile.dataValues

    if (userProfile.type !== 'client') return res.status(403).json({
        message: 'invalid user'
    })

    const job = await jobDbRepository.getClientJobWithContract(job_id, userProfile.id)

    if (!job) return res.status(422).json({
        message: 'invalid job'
    })

    if (job.paid) return res.status(422).json({
        message: 'paid job'
    })

    if (userProfile.balance < job.price) return res.status(422).json({
        message: 'invalid balance'
    })

    const result = await jobDbRepository.payClientJob(job_id, job.dataValues.price, userProfile.id, job.dataValues.Contract.ContractorId)

    if (result.completed) res.json()

    if (!result.completed) res.status(503).json({
        message: result.message
    })

})

module.exports = app;
