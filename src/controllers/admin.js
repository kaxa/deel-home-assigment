const {getProfile} = require('../middleware/getProfile')
const {Router} = require('express');
const app = Router();

/**
 * Returns the profession that earned the most money
 * (sum of jobs paid) for any contactor that worked in the query time range
 * @returns contract by id
 */
app.get('/admin/best-profession', getProfile, async (req, res) => {

    const startDate = req.query.start
    const endDate = req.query.end

    if (!startDate || !endDate) res.status(400).json({message: 'invalid request'})

    const profileDbRepository = req.app.get('profileDbRepository')

    const result = await profileDbRepository.getBestProfession(new Date(startDate), new Date(endDate))

    if (!result) res.status(503).json({message: 'invalid range'})

    res.json(result?.Contract?.Contractor?.profession)

})

/**
 * returns the clients the paid the most for jobs in the query time period
 * @returns contract by id
 */
app.get('/admin/best-clients', getProfile, async (req, res) => {

    const startDate = req.query.start
    const endDate = req.query.end
    const limit = +(req.query.limit) || 2

    if (!startDate || !endDate) res.status(400).json({message: 'invalid request'})

    const profileDbRepository = req.app.get('profileDbRepository')

    const result = await profileDbRepository.getBestClients(new Date(startDate), new Date(endDate), limit)

    if (!result) res.status(503).json({message: 'invalid range'})

    res.json(result.map(x => {
        return {
            paid: x.dataValues.total,
            fullName: `${x.Contract.Client.firstName} ${x.Contract.Client.lastName}`,
            id: x.Contract.Client.id
        }
    }))

})

module.exports = app;
