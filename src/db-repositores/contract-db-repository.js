const {sequelize} = require('../model')
const {Op} = require("sequelize");

Contract = sequelize.models.Contract

class ContractDbRepository {

    async getContract(id) {
        return Contract.findOne({where: {id}})
    }

    async listClientUnTerminatedContracts(clientId) {
        return Contract.findAll({
            where: {
                ClientId: clientId,
                status: {
                    [Op.not]: 'terminated'
                }
            }
        })
    }

    async listContractorUnTerminatedContracts(contractorId) {
        return Contract.findAll({
            where: {
                ContractorId: contractorId,
                status: {
                    [Op.not]: 'terminated'
                }
            }
        })
    }

}

module.exports = {ContractDbRepository}
