const {sequelize} = require('../model')

Job = sequelize.models.Job
Contract = sequelize.models.Contract
Profile = sequelize.models.Profile

class JobDbRepository {

    async getClientJobWithContract(id, clientId) {

        return Job.findOne({
            where: {id},
            include: [
                {
                    model: Contract,
                    as: 'Contract',
                    attributes: ['ContractorId'],
                    where: {
                        ClientId: clientId,
                        // todo  can terminated or new contract jobs be paid ?
                        // status: 'in_progress'
                    }
                }
            ]
        })

    }

    async getClientUnpaidJobs(clientId) {
        return Job.findAll({
            where: {
                paid: null
            },
            include: [
                {
                    model: Contract,
                    as: 'Contract',
                    attributes: [],
                    required: true,
                    where: {
                        ClientId: clientId,
                        status: 'in_progress'
                    }
                }
            ]
        })
    }

    async getContractorUnpaidJobs(ContractorId) {
        return Job.findAll({
            where: {
                paid: null
            },
            include: [
                {
                    model: Contract,
                    as: 'contract',
                    attributes: [],
                    required: true,
                    where: {
                        ContractorId: ContractorId,
                        status: 'in_progress'
                    }
                }
            ]
        })
    }

    async payClientJob(jobId, amount, clientId, contractorId) {

        const t = await sequelize.transaction();

        try {

            await Profile.findOne({
                where: {
                    id: clientId,
                },
                lock: true,
                skipLocked: true,
                transaction: t
            })

            await Profile.findOne({
                where: {
                    id: contractorId,
                },
                lock: true,
                skipLocked: true,
                transaction: t
            })

            await Job.findOne({
                lock: true,
                skipLocked: true,
                transaction: t,
                where: {
                    id: jobId
                }
            })

            await Job.update(
                {paid: 1, paymentDate: new Date().toISOString()},
                {
                    where: {id: jobId},
                    transaction: t
                }
            );

            await Profile.update(
                {
                    balance: sequelize.literal(`balance - ${amount}`)
                },
                {
                    where: {
                        id: clientId
                    },
                    transaction: t
                });

            await Profile.update(
                {
                    balance: sequelize.literal(`balance + ${amount}`)
                },
                {
                    where: {
                        id: contractorId
                    },
                    transaction: t
                }
            );

            await t.commit();
            return {completed: true}

        } catch (error) {

            console.log("error", error)

            await t.rollback();

            return {completed: false, message: "unknown error"}

        }

    }

}

module.exports = {JobDbRepository}
