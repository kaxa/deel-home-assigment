const {sequelize} = require('../model')

Job = sequelize.models.Job
Contract = sequelize.models.Contract
Profile = sequelize.models.Profile

class BalanceDbRepository {

    async depositToUser(payerId, receiverId, amount) {

        const t = await sequelize.transaction();

        try {

            await Profile.findOne({
                where: {
                    id: payerId,
                },
                lock: true,
                skipLocked: true,
                transaction: t
            })

            const receiverUser = await Profile.findOne({
                where: {
                    id: receiverId,
                },
                lock: true,
                skipLocked: true,
                transaction: t
            })

            if (!receiverUser) return {completed: false, message: 'invalid user'}

            await Job.findAll({
                where: {
                    paid: null
                },
                include: [
                    {
                        model: Contract,
                        as: 'Contract',
                        attributes: [],
                        required: true,
                        where: {
                            ClientId: payerId
                        }
                    }
                ],
                lock: true,
                skipLocked: true,
                transaction: t
            })

            const userAmount = await Job.findAll({
                    attributes: [[sequelize.fn('sum', sequelize.col('price')), 'total']],
                    where: {
                        paid: null
                    },
                    include: [
                        {
                            model: Contract,
                            as: 'Contract',
                            attributes: [],
                            required: true,
                            where: {
                                ClientId: payerId
                            }
                        }
                    ],
                    transaction: t
                }
            )

            let toPayAmount = userAmount[0].dataValues.total

            if (toPayAmount == null) {
                toPayAmount = 0
            }

            if (amount > (toPayAmount * 25) / 100) {

                return {completed: false, message: 'invalid amount - unpaid jobs'}
            }

            await Profile.update(
                {
                    balance: sequelize.literal(`balance - ${amount}`)
                },
                {
                    where: {
                        id: payerId
                    },
                    transaction: t
                }
            );

            await Profile.update(
                {
                    balance: sequelize.literal(`balance + ${amount}`)
                },
                {
                    where: {
                        id: receiverId
                    },
                    transaction: t
                }
            );

            await t.commit();

            return {completed: true}

        } catch (error) {

            console.log("error", error)

            await t.rollback();

            return {completed: false, message: "unknown error"}

        }

    }

}

module.exports = {BalanceDbRepository}
