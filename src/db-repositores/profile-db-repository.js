const {sequelize} = require('../model')
const {Op} = require("sequelize");

Profile = sequelize.models.Profile
Job = sequelize.models.Job
Contract = sequelize.models.Contract

class ProfileDbRepository {

    async getBestProfession(start, end) {

        return   await Job.findOne({
            group: [sequelize.col('Contract.Contractor.profession')],
            order: [
                ['total', 'DESC'],
            ],
            limit: 1,
            attributes: [
                sequelize.col('Contract.Contractor.profession'),
                [sequelize.fn('sum', sequelize.col('price')), 'total'],
            ],
            where: {
                paid: 1,
                createdAt: {
                    [Op.between]: [start, end]
                }
            },
            include: [
                {
                    model: Contract,
                    as: 'Contract',
                    attributes: ['id'],
                    include: [
                        {
                            model: Profile,
                            attributes: ['profession'],
                            as: 'Contractor'
                        }
                    ]
                }
            ]
        })



    }

    async getBestClients(start, end, limit) {

        return await Job.findAll({
            group: [sequelize.col('Contract.Client.id')],
            order: [
                ['total', 'DESC'],
            ],
            limit: limit,
            attributes: [
                sequelize.col('Contract.Client.id'),
                [sequelize.fn('sum', sequelize.col('price')), 'total'],
            ],
            where: {
                paid: 1,
                createdAt: {
                    [Op.between]: [start, end]
                }
            },
            include: [
                {
                    model: Contract,
                    as: 'Contract',
                    attributes: ['id'],
                    include: [
                        {
                            model: Profile,
                            attributes: ['id', 'firstName', 'lastName'],
                            as: 'Client'
                        }
                    ]
                }
            ]
        })

    }

}

module.exports = {ProfileDbRepository}
