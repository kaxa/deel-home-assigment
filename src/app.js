const express = require('express');
const bodyParser = require('body-parser');

const contractController = require('./controllers/contract')
const jobController = require('./controllers/job')
const balanceController = require('./controllers/balance')
const adminController = require('./controllers/admin')

const {ContractDbRepository} = require('./db-repositores/contract-db-repository')
const {JobDbRepository} = require('./db-repositores/job-db-repository')
const {BalanceDbRepository} = require('./db-repositores/balance-db-repository')
const {ProfileDbRepository} = require('./db-repositores/profile-db-repository')

const {sequelize} = require("./model");
const app = express();
app.use(bodyParser.json());
app.use(contractController);
app.use(jobController);
app.use(balanceController);
app.use(adminController);

app.set('sequelize', sequelize)
app.set('models', sequelize.models)
app.set('contractDbRepository', new ContractDbRepository())
app.set('jobDbRepository', new JobDbRepository())
app.set('balanceDbRepository', new BalanceDbRepository())
app.set('profileDbRepository', new ProfileDbRepository())

module.exports = app;
